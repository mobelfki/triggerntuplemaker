//
// Header files
//


#ifndef NTUPLEMAKER_NTUPLEMAKE_H
#define NTUPLEMAKER_NTUPLEMAKE_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "AthenaBaseComps/AthAlgTool.h"
#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ToolHandle.h"
#include "StoreGate/ReadHandle.h"
#include "CaloEvent/CaloCellContainer.h"
#include "TrigAnalysisInterfaces/IBunchCrossingTool.h"
#include "xAODTrigL1Calo/TriggerTowerContainer.h"
#include "xAODTrigL1Calo/JGTowerContainer.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TriggerMatchingTool/IMatchingTool.h"
#include "AsgTools/AnaToolHandle.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODEventShape/EventShape.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "TTree.h"
#include "CaloInterface/ICaloNoiseTool.h"

using namespace std;

class NTupleMaker : public :: AthAnalysisAlgorithm { 

public: 

	NTupleMaker( const std::string& name, ISvcLocator* pSvcLocator );
	
	virtual ~NTupleMaker();
	
	virtual StatusCode initialize();
	virtual StatusCode execute();
	virtual StatusCode finalize();
	
public: 
            std::string m_CellContainerName;
            std::string m_SCellContainerName;
            
            bool       m_useSC;
            bool 	m_isMC;
            bool  	m_saveAllInfo;
            bool 	m_saveTowers;
            bool       m_saveimage;
            bool       m_saveCells;
            bool       m_applyNoiseCorrection;
            
            std::vector<std::string> m_HLT_MET;
            std::vector<std::string> m_Triggers;
            std::vector<std::string> m_Truth_MET = {"NonInt", "Int", "IntMuons", "NonIntPlusIntMuons", "NonIntMinusIntMuons"};
            std::vector<int> m_CaloSampling;
private: 

	
	
	TTree* m_tree = 0;
	
	std::vector<double> cells_et;	
	std::vector<double> cells_eta;
	std::vector<double> cells_phi;
	std::vector<double> cells_t;
	std::vector<int>   cells_q;
	std::vector<int>   cells_smpl;
	std::vector<bool>  cells_pT;
	std::vector<bool>  cells_PF;
	
	std::vector<double> g_towers_et;	
	std::vector<double> g_towers_eta;
	std::vector<double> g_towers_phi;
	std::vector<int>   g_towers_smpl;

	std::vector<double> j_towers_et;	
	std::vector<double> j_towers_eta;
	std::vector<double> j_towers_phi;
	std::vector<int>   j_towers_smpl;
	
	std::map<TString, double> hlt_met_ex;
	std::map<TString, double> hlt_met_ey; 
	std::map<TString, double> hlt_met_et;  
	
	std::map<TString, double> truth_met_ex;
	std::map<TString, double> truth_met_ey; 
	std::map<TString, double> truth_met_et; 
	
	std::map<TString, TH2D*> SamplingTH2D;
	std::map<TString, std::vector<double>> SamplingImage;
            
	double mu;
	int   evtNumber;
	double evtWeight;
	double evtLiveTime;
	double evtDensity;
	double evtDensitySigma;
	double evtArea;
	bool  isGoodLB = true;
	double distFrontBunchTrain; //

	double evtCellsEt;
	double evtCellsEx;
	double evtCellsEy;
    
        int* TrigDecisions;
private: 
	ToolHandle<Trig::TrigDecisionTool> m_TrigDecTool;        
	ToolHandle<Trig::IBunchCrossingTool> m_bcTool;   
	ToolHandle<ICaloNoiseTool> m_noiseTool;
	
private: 
	
	inline void clear() {
	
		cells_et.clear();	
        	cells_eta.clear();
        	cells_phi.clear();
        	cells_t.clear();
        	cells_q.clear();
        	cells_smpl.clear();
        	cells_pT.clear();
        	cells_PF.clear();
		
		g_towers_et.clear();	
        	g_towers_eta.clear();
        	g_towers_phi.clear();
        	g_towers_smpl.clear();
        	
        	j_towers_et.clear();	
        	j_towers_eta.clear();
        	j_towers_phi.clear();
        	j_towers_smpl.clear();
	
	}
	
	StatusCode RetrieveHLTMET(TString name);
	void RetrieveTruthMET(const std::string &term, const xAOD::MissingET &met);  
};

#endif //> !NTUPLEMAKER_NTUPLEMAKER_H
