#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../NTupleMaker.h"
DECLARE_ALGORITHM_FACTORY( NTupleMaker )

DECLARE_FACTORY_ENTRIES( NTupleMaker ) 
{
  DECLARE_ALGORITHM( NTupleMaker );

}

