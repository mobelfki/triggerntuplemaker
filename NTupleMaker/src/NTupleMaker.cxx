//

//

#include "NTupleMaker.h"


NTupleMaker::NTupleMaker( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){

        declareProperty("CellContainerName", m_CellContainerName="AllCalo");
        declareProperty("SCellContainerName", m_SCellContainerName="SCalo");
        declareProperty("useSuperCells", m_useSC = true);
        declareProperty("CaloNoiseTool", m_noiseTool);
        declareProperty("saveAllInfo", m_saveAllInfo = false);
        declareProperty("saveTowers", m_saveTowers = false);
        declareProperty("HLT_MET", m_HLT_MET);
        declareProperty("Triggers", m_Triggers);
        declareProperty("isMC", m_isMC = true);
        declareProperty("SaveImage", m_saveimage = true);
        declareProperty("SaveCells", m_saveCells = true);
        declareProperty("applyNoiseCorrection", m_applyNoiseCorrection = false);
        declareProperty("CaloSampling", m_CaloSampling);
}

NTupleMaker::~NTupleMaker() {

}

StatusCode NTupleMaker::initialize() {

    ATH_MSG_INFO ("Initializing " << name() << "...");
    
    m_TrigDecTool.setTypeAndName("Trig::TrigDecisionTool/TrigDecisionTool");
    CHECK( m_TrigDecTool.retrieve() );
    
    m_bcTool.setTypeAndName("Trig::MCBunchCrossingTool/BunchCrossingTool");
    CHECK( m_bcTool.retrieve() );
    
    ServiceHandle<ITHistSvc> histSvc("THistSvc",name());
    CHECK( histSvc.retrieve() );
    
    //m_noiseTool.setTypeAndName("CaloNoiseTool/CaloNoiseToolDefault");
    CHECK( m_noiseTool.retrieve());
    
    m_tree = new TTree("NTUP","NTupleMaker");
    
    std::string Label = "Cells";
    if(m_useSC) Label = "SCells";

    m_tree->Branch("Event.Number",              &evtNumber);
    m_tree->Branch("Event.Weight",              &evtWeight);
    m_tree->Branch("Event.LiveTime",            &evtLiveTime);
    m_tree->Branch("Event.Density",             &evtDensity);
    m_tree->Branch("Event.DensitySigma",        &evtDensitySigma);
    m_tree->Branch("Event.Area",                &evtArea);
    m_tree->Branch("Event.isGoodLB",            &isGoodLB);
    m_tree->Branch("Event.distFrontBunchTrain", &distFrontBunchTrain);
    m_tree->Branch("Event.mu",                  &mu);
       
    m_tree->Branch(Form("%s.et",Label.data()),            &evtCellsEt);
    m_tree->Branch(Form("%s.ex",Label.data()),            &evtCellsEx);
    m_tree->Branch(Form("%s.ey",Label.data()),            &evtCellsEy);       
    
    if(m_saveCells) {   
        
	    m_tree->Branch(Form("%s.eT",Label.data()),       &cells_et); // super cells eT 
	    m_tree->Branch(Form("%s.Eta",Label.data()),      &cells_eta); // super cells eta 
	    m_tree->Branch(Form("%s.Phi",Label.data()),      &cells_phi); // super cells phi
	    m_tree->Branch(Form("%s.Sampling",Label.data()), &cells_smpl); // super cells layer
    }
    
    if(m_saveAllInfo) {
    	m_tree->Branch(Form("%s.Time",Label.data()),     &cells_t); // super cells time
    	m_tree->Branch(Form("%s.Qualtiy",Label.data()),  &cells_q);
    	m_tree->Branch(Form("%s.passTime",Label.data()), &cells_pT); 
    	m_tree->Branch(Form("%s.passPF",Label.data()),   &cells_PF); 
    }
    
    if(m_saveTowers) {
    
	    m_tree->Branch("gTowers.ET",       &g_towers_et);
	    m_tree->Branch("gTowers.Eta",      &g_towers_eta);
	    m_tree->Branch("gTowers.Phi",      &g_towers_phi);
	    m_tree->Branch("gTowers.Sampling", &g_towers_smpl);
	    
	    m_tree->Branch("jTowers.ET",       &j_towers_et);
	    m_tree->Branch("jTowers.Eta",      &j_towers_eta);
	    m_tree->Branch("jTowers.Phi",      &j_towers_phi);
	    m_tree->Branch("jTowers.Sampling", &j_towers_smpl);
    }
    
    for(unsigned int i = 0; i<m_HLT_MET.size(); i++) {
    
    	hlt_met_ex[m_HLT_MET[i]] = 0;
    	hlt_met_ey[m_HLT_MET[i]] = 0;
    	hlt_met_et[m_HLT_MET[i]] = 0;
    	
    	m_tree->Branch(Form("%s.ex",m_HLT_MET[i].data()),  &hlt_met_ex[m_HLT_MET[i]]);
    	m_tree->Branch(Form("%s.ey",m_HLT_MET[i].data()),  &hlt_met_ey[m_HLT_MET[i]]);
    	m_tree->Branch(Form("%s.et",m_HLT_MET[i].data()),  &hlt_met_et[m_HLT_MET[i]]);
    }
    
    if(m_isMC){
    
	    for(unsigned int i = 0; i<m_Truth_MET.size(); i++) {
	    
	    	truth_met_ex[m_Truth_MET[i]] = 0;
	    	truth_met_ey[m_Truth_MET[i]] = 0;
	    	truth_met_et[m_Truth_MET[i]] = 0;
	    
	   	m_tree->Branch(Form("Truth_MET_%s.ex", m_Truth_MET[i].data()), &truth_met_ex[m_Truth_MET[i]]);
		m_tree->Branch(Form("Truth_MET_%s.ey", m_Truth_MET[i].data()), &truth_met_ey[m_Truth_MET[i]]);			 
		m_tree->Branch(Form("Truth_MET_%s.et", m_Truth_MET[i].data()), &truth_met_et[m_Truth_MET[i]]);			 
		    
	    }
    }
   
    TrigDecisions = new int [m_Triggers.size()];
    for(unsigned int i = 0; i<m_Triggers.size(); i++) {
    	
    		TrigDecisions[i] = 0;
    		m_tree->Branch(Form("TrigDecision_%s", m_Triggers[i].data()), &(TrigDecisions[i]));
    }
    
    if(m_saveimage) {
    
	for(unsigned int i = 0; i <m_CaloSampling.size(); i++) {
	   	SamplingTH2D[ Form("Sampling%i", m_CaloSampling[i]) ] = new TH2D( Form("Sampling%i", m_CaloSampling[i]), Form("Sampling%i", m_CaloSampling[i]), 50, -2.5, 2.5, 64, -3.2, 3.2   );
	   	CHECK( histSvc->regHist(Form("/OUTPUT/Hist_Sampling%i", m_CaloSampling[i]), SamplingTH2D[ Form("Sampling%i", m_CaloSampling[i]) ]));
	    }
    
	for(unsigned int i = 0; i <m_CaloSampling.size(); i++) {
		
		SamplingImage[Form("ImageSampling%i", m_CaloSampling[i])].reserve(3200);
		SamplingImage[Form("ImageSampling%i", m_CaloSampling[i])].clear();
		m_tree->Branch(Form("ImageSampling%i", m_CaloSampling[i]),  &SamplingImage[Form("ImageSampling%i", m_CaloSampling[i])]);
	}
    }
    
           
    CHECK( histSvc->regTree("/OUTPUT/Tree", m_tree) );
    
    
    return StatusCode::SUCCESS;
}

StatusCode NTupleMaker::finalize() {
    ATH_MSG_INFO ("Finalizing " << name() << "...");
    
    delete [] TrigDecisions;
    TrigDecisions = NULL;
    return StatusCode::SUCCESS;
    
}

StatusCode NTupleMaker::execute() {

	ATH_MSG_DEBUG ("Executing " << name() << "...");
   
	clear();
	
	if(m_saveimage) {
	for(unsigned int i = 0; i <m_CaloSampling.size(); i++) {
   		SamplingTH2D[ Form("Sampling%i", m_CaloSampling[i]) ]->Reset();
   		SamplingImage[ Form("ImageSampling%i", m_CaloSampling[i]) ].clear();
    	}
	}
	
	const xAOD::EventInfo* ei = 0;
  	CHECK( evtStore()->retrieve( ei , "EventInfo" ));
  	
 	mu        = ei->averageInteractionsPerCrossing(); 
  	evtNumber = ei->eventNumber();
  	

	if( not m_isMC) {
	
		evtLiveTime = ei->auxdata<double>("EnhancedBiasLivetime");
		evtWeight   = ei->auxdata<double>("EnhancedBiasWeight");
		isGoodLB    = static_cast<bool> (ei->auxdata<char>("IsGoodLBFlag") );
	}
	

  	distFrontBunchTrain = m_bcTool->distanceFromFront(ei->bcid(), Trig::IBunchCrossingTool::BunchCrossings);

	const xAOD::EventShape* eventShape = 0;
  	CHECK( evtStore()->retrieve( eventShape, "Kt4EMTopoOriginEventShape" ) );
  	
  	evtDensity      = eventShape->getDensity( xAOD::EventShape::Density );
  	evtArea         = eventShape->getDensity( xAOD::EventShape::DensityArea );
  	evtDensitySigma = eventShape->getDensity( xAOD::EventShape::DensitySigma );
  	
  	for( unsigned i = 0; i<m_Triggers.size(); i++) {
  	
  		int passTrigger = m_TrigDecTool->isPassed(m_Triggers[i]);
  		TrigDecisions[i] = passTrigger;
  	}

	const CaloCellContainer* Cells = 0;
	
	std::string CellsContainer;
	
	if(m_useSC) {
		ATH_MSG_INFO (" Retrieve " << m_SCellContainerName.data() << " ... " );
		CellsContainer = m_SCellContainerName.data();
	} else {
	
		ATH_MSG_INFO (" Retrieve " << m_CellContainerName.data() << " ... " );
		CellsContainer = m_CellContainerName.data();
	}
	
	CHECK( evtStore()->retrieve(Cells, CellsContainer.data()));
        	
       	
       	evtCellsEt = 0;
       	evtCellsEx = 0;
       	evtCellsEy = 0;
       	
	for( const auto cell : *Cells) {
	
		if(m_applyNoiseCorrection) {
		double rms = m_noiseTool->getNoise(cell, ICalorimeterNoiseTool::TOTALNOISE);
		double E = cell->e();
		if (3.0 > fabsf(2.0)) {
          	// 1-sided cut
          		if (E < 3.0 * rms) continue;
       		} else if (3.0 < -fabsf(2.0)) {
         		 // mixed case
         		 	if (E < 3.0 * rms ||     // 1-sided cut
              			fabs(E) < 2.0 * rms) // 2-sided cut
             			continue;
       		} else {
          		// 2-sided cut: never use this online!
          		if (fabs(E) < 2.0 * rms) continue;
       		}
	
		}
		
		float cell_ex = cell->e()*(cell->caloDDE()->sinTh())*(cell->caloDDE()->cosPhi());
		float cell_ey = cell->e()*(cell->caloDDE()->sinTh())*(cell->caloDDE()->sinPhi());
		
		float cell_et = sqrt( cell_ex*cell_ex + cell_ey*cell_ey);
		
		if(m_saveCells) {

			cells_et.push_back(cell_et);
			cells_smpl.push_back(cell->caloDDE()->getSampling());
			cells_eta.push_back(cell->caloDDE()->eta());
			cells_phi.push_back(cell->caloDDE()->phi());
		}
		
		
		if(m_saveimage) {
			for(unsigned int i = 0; i <m_CaloSampling.size(); i++) {
				if(m_CaloSampling[i] != cell->caloDDE()->getSampling()) continue;
				SamplingTH2D[ Form("Sampling%i", cell->caloDDE()->getSampling()) ]->Fill(cell->caloDDE()->eta(), cell->caloDDE()->phi(), cell_et);
			}
		}
		
		
		
		evtCellsEx -= cell_ex;
		evtCellsEy -= cell_ey;
		
		if(m_saveAllInfo) {
			cells_t.push_back(cell->time());
			cells_q.push_back(cell->quality());
			bool passTime = cell->provenance() & 0x200;
	        		bool passPF   = cell->provenance() & 0x40;
			cells_pT.push_back(passTime);
			cells_PF.push_back(passPF);
		}
	}
	
	if(m_saveimage) {
	
		for(unsigned int i = 0; i<m_CaloSampling.size(); i++) {
			
			int NPhi = SamplingTH2D[ Form("Sampling%i", m_CaloSampling[i]) ]->GetNbinsY();
			int NEta = SamplingTH2D[ Form("Sampling%i", m_CaloSampling[i]) ]->GetNbinsX();
			for(int phi = 1; phi <= NPhi; phi++) {
			for(int eta = 1; eta <= NEta; eta++) {
				SamplingImage[Form("ImageSampling%i", m_CaloSampling[i])].push_back( SamplingTH2D[ Form("Sampling%i", m_CaloSampling[i]) ]->GetBinContent(eta, phi));
			
			}
			}
		
		}
	
	}
	
	evtCellsEt = sqrt( evtCellsEx*evtCellsEx + evtCellsEy*evtCellsEy);
	
	if(m_saveTowers) {
	
		const xAOD::JGTowerContainer* gTowers =0;
		CHECK( evtStore()->retrieve( gTowers,"GCaloTowers"));

		const xAOD::JGTowerContainer* jTowers =0;
		CHECK( evtStore()->retrieve( jTowers,"JTowers"));
	
		for( const auto tower : *gTowers) {
		
			g_towers_et.push_back(tower->et());
			g_towers_smpl.push_back(tower->sampling());
			g_towers_eta.push_back(tower->eta());
			g_towers_phi.push_back(tower->phi());
		} 	
		
		for( const auto tower : *jTowers) {
		
			j_towers_et.push_back(tower->et());
			j_towers_smpl.push_back(tower->sampling());
			j_towers_eta.push_back(tower->eta());
			j_towers_phi.push_back(tower->phi());
		}
		
	}	
	
	for(unsigned int i=0; i<m_HLT_MET.size();i++) CHECK(RetrieveHLTMET(m_HLT_MET[i]));
	
	if(m_isMC) {
		
	    const xAOD::MissingETContainer* MET_Truth = 0;
	    CHECK( evtStore()->retrieve( MET_Truth, "MET_Truth" ) );
	
	    const xAOD::MissingET &nonIntMET = *((*MET_Truth)["NonInt"]);
	    const xAOD::MissingET &muonsMET  = *((*MET_Truth)["IntMuons"]);
	    const xAOD::MissingET &intMET    = *((*MET_Truth)["Int"]);
	    
	    RetrieveTruthMET("Int", intMET);
	    RetrieveTruthMET("NonInt", nonIntMET);
	    RetrieveTruthMET("IntMuons", muonsMET);
	    RetrieveTruthMET("NonIntPlusIntMuons", nonIntMET + muonsMET);
	    RetrieveTruthMET("NonIntMinusIntMuons", nonIntMET - muonsMET);
	
	    evtWeight = ei->mcEventWeights()[0];
	
	}
	
	m_tree->Fill();
	
	return StatusCode::SUCCESS;
}


void NTupleMaker::RetrieveTruthMET(const std::string &term, const xAOD::MissingET &met)
{
	
	truth_met_ex[term] = met.mpx();
  	truth_met_ey[term] = met.mpy();
  	truth_met_et[term] = met.met();
	
  	return;
}


StatusCode NTupleMaker::RetrieveHLTMET(TString name){

	const xAOD::TrigMissingETContainer* met(0); 
  	CHECK( evtStore()->retrieve(met, name.Data() ));

  	float ex = met->front()->ex(); 
  	float ey = met->front()->ey();

  	hlt_met_ex[name] = ex;
  	hlt_met_ey[name] = ey;
  	hlt_met_et[name] = sqrt(ex*ex + ey*ey);
  
  	return StatusCode::SUCCESS;

}

