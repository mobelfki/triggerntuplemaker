
from PerfMonComps.PerfMonFlags import jobproperties
jobproperties.PerfMonFlags.doMonitoring = False
from Digitization.DigitizationFlags import digitizationFlags
#Some generic setup stuff

theApp.EvtMax=10

#files=['/eos/atlas/user/b/bcarlson//Run3Tmp/mc16_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.recon.ESD.e3601_s3126_r12406/ESD.24368006._000029.pool.root.1']

files=['/eos/user/m/mobelfki/AOD/ESD/mc16_13TeV/ESD.19647790._000246.pool.root.1']


debug = False
doMonitoring=False
doFexAlgos=False
isESD=True

#Input file
from PyUtils import AthFile
import AthenaPoolCnvSvc.ReadAthenaPool                 #sets up reading of POOL files (e.g. xAODs)
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags
svcMgr.EventSelector.InputCollections=files

###
#svcMgr.EventSelector.InputCollections=['/eos/atlas/user/b/bcarlson/Run3Tmp/data18_13TeV.00360026.physics_EnhancedBias.recon.ESD.r10978_r11179_r11185/ESD.16781883._001043.pool.root.1']
#I have one reference file to come back to 

athenaCommonFlags.FilesInput = svcMgr.EventSelector.InputCollections


from RecExConfig.InputFilePeeker import inputFileSummary
print ("Input type: %s" % inputFileSummary['stream_names'])

if("StreamAOD" in inputFileSummary['stream_names']):
    print "Processing an AOD file and disable some settings for reading an ESD, and do not run Run 3 FEX algorithms"
    isESD=False
    doFexAlgos=False


from AthenaCommon.AlgSequence import AlgSequence
topSequence=AlgSequence()

af = AthFile.fopen(svcMgr.EventSelector.InputCollections[0])
isMC = 'IS_SIMULATION' in af.fileinfos['evt_type']
run_number = af.run_number[0]

if(isESD):
    #Some database settings, needed for ESD 
    from RecExConfig import AutoConfiguration
    AutoConfiguration.ConfigureSimulationOrRealData()
    AutoConfiguration.ConfigureGeo()
    
    from AthenaCommon.DetFlags import DetFlags
    DetFlags.detdescr.all_setOff() 
    DetFlags.detdescr.Calo_setOn()
    include("RecExCond/AllDet_detDescr.py")

#What does this do 
if(isMC and isESD ):
    include( "LArConditionsCommon/LArIdMap_MC_jobOptions.py" )
    
#What does this do 
from RegistrationServices.RegistrationServicesConf import IOVRegistrationSvc
svcMgr += IOVRegistrationSvc()
if debug:
    svcMgr.IOVRegistrationSvc.OutputLevel = DEBUG
else:
    svcMgr.IOVRegistrationSvc.OutputLevel = INFO
svcMgr.IOVRegistrationSvc.RecreateFolders = True
svcMgr.IOVRegistrationSvc.SVFolder = False
svcMgr.IOVRegistrationSvc.userTags = False

#These lines are for configuring the bunch crossing tool, useful for some cases
from TrigBunchCrossingTool.BunchCrossingTool import BunchCrossingTool
if isMC: ToolSvc += BunchCrossingTool( "MC" )
else: ToolSvc += BunchCrossingTool( "LHC" )

'''
include("LArConditionsCommon/LArConditionsCommon_MC_jobOptions.py")
from LArDigitization.LArDigitizationConf import LArDigitMaker
digitmaker1 = LArDigitMaker("LArDigitSCL1")
topSequence += digitmaker1
digitizationFlags.rndmSeedList.addSeed("LArDigitization", 1234, 5678 )

include("LArROD/LArConfigureCablingSCFolder.py")
from LArL1Sim.LArSCL1Getter import *

theLArSCL1Getter = LArSCL1Getter()
digitizationFlags.rndmSeedList.addSeed("LArSCL1Maker", 335242, 7306589 )

theLArSCL1Maker=theLArSCL1Getter.LArSCL1Maker()
theLArSCL1Maker.RndmSvc = digitizationFlags.rndmSvc.get_Value()

from LArSCellGetter import LArSCellGetter
theLArSCellGetter = LArSCellGetter()
'''


from CaloTools.CaloNoiseToolDefault import CaloNoiseToolDefault
theCaloNoiseTool=CaloNoiseToolDefault()
ToolSvc+=theCaloNoiseTool

#Calibrate jets 


from METUtilities.METMakerConfig import getMETMakerAlg
metAlg = getMETMakerAlg('AntiKt4EMTopo',"Loose")
metAlg.METName = 'MET_Reco_AntiKt4EMTopo'

ToolSvc += CfgMgr.JetCalibrationTool("myJESTool")
ToolSvc.myJESTool.IsData=False
ToolSvc.myJESTool.ConfigFile="JES_MC16Recommendation_Aug2017.config"
ToolSvc.myJESTool.CalibSequence="JetArea_Residual_EtaJES_GSC"
ToolSvc.myJESTool.JetCollection="AntiKt4EMTopo" 
#ToolSvc += CfgMgr.Trig__MatchingTool("MyMatchingTool",OutputLevel=DEBUG)

ToolSvc += CfgMgr.Trig__MatchingTool("MyMatchingTool",OutputLevel=DEBUG)
ToolSvc += CfgMgr.CP__MuonSelectionTool("MediumMuonTool", MuQuality=1) # 1 corresponds to Medium
ToolSvc += CfgMgr.JetVertexTaggerTool('JVT')

ToolSvc += CfgMgr.Trig__TrigDecisionTool( "TrigDecisionTool" )


if(isMC):
    #Truth jets
    from DerivationFrameworkCore.DerivationFrameworkMaster import *
    from DerivationFrameworkMCTruth.MCTruthCommon import * 

    from JetRec.JetAlgorithm import addJetRecoToAlgSequence
    addJetRecoToAlgSequence(topSequence,eventShapeTools=None)

    from JetRec.JetRecFlags import jetFlags
    jetFlags.useTruth = True

    from JetRec.JetRecStandard import jtm
    from JetRec.JetRecConf import JetAlgorithm

    jetFlags.truthFlavorTags = ["BHadronsInitial", "BHadronsFinal", "BQuarksFinal",
                                "CHadronsInitial", "CHadronsFinal", "CQuarksFinal",
                                "TausFinal",
                                "Partons",
                                ]
    akt4 = jtm.addJetFinder("AntiKt4TruthJets", "AntiKt", 0.4, "truth", 
                            modifiersin=[jtm.truthpartondr, jtm.partontruthlabel, jtm.removeconstit, jtm.jetdrlabeler, jtm.trackjetdrlabeler], 
                            ptmin= 5000)
    akt10 = jtm.addJetFinder("AntiKt10TruthJets", "AntiKt", 1.0, "truth",
                            modifiersin=[jtm.truthpartondr, jtm.partontruthlabel, jtm.removeconstit, jtm.jetdrlabeler, jtm.trackjetdrlabeler],
                            ptmin= 5000)
    akt4alg = JetAlgorithm("jetalgAntiKt4TruthJets", Tools = [akt4] )
    akt10alg = JetAlgorithm("jetalgAntiKt10TruthJets", Tools = [akt10] )
    topSequence +=  akt4alg 
    topSequence +=  akt10alg


    # Try using TAT to build truth taus?
    DFCommonTauTruthWrapperTools = []

    from MCTruthClassifier.MCTruthClassifierConf import MCTruthClassifier
    DFCommonTauTruthClassifier = MCTruthClassifier(name = "DFCommonTauTruthClassifier",
                                        ParticleCaloExtensionTool="")

    ToolSvc += DFCommonTauTruthClassifier

    from DerivationFrameworkMCTruth.DerivationFrameworkMCTruthConf import DerivationFramework__TruthCollectionMakerTau
    DFCommonTruthTauTool = DerivationFramework__TruthCollectionMakerTau(name             = "DFCommonTruthTauTool",
                                                                        NewCollectionName       = "TruthTaus",
                                                                        MCTruthClassifier       = DFCommonTauTruthClassifier)
    ToolSvc += DFCommonTruthTauTool
    DFCommonTauTruthWrapperTools.append(DFCommonTruthTauTool)

    from TauAnalysisTools.TauAnalysisToolsConf import TauAnalysisTools__TauTruthMatchingTool
    from DerivationFrameworkTau.DerivationFrameworkTauConf import DerivationFramework__TauTruthMatchingWrapper
    DFCommonTauTruthMatchingTool = TauAnalysisTools__TauTruthMatchingTool(name="DFCommonTauTruthMatchingTool")
    ToolSvc += DFCommonTauTruthMatchingTool
    DFCommonTauTruthMatchingWrapper = DerivationFramework__TauTruthMatchingWrapper( name = "DFCommonTauTruthMatchingWrapper",
                                                                                    TauTruthMatchingTool = DFCommonTauTruthMatchingTool,
                                                                                    TauContainerName     = "TauJets")
    ToolSvc += DFCommonTauTruthMatchingWrapper
    print DFCommonTauTruthMatchingWrapper
    DFCommonTauTruthWrapperTools.append(DFCommonTauTruthMatchingWrapper)

    from DerivationFrameworkCore.DerivationFrameworkCoreConf import DerivationFramework__CommonAugmentation
    topSequence += CfgMgr.DerivationFramework__CommonAugmentation("TauTruthCommonKernel",
                                                                AugmentationTools = DFCommonTauTruthWrapperTools)


#Create an output root file 

jps.AthenaCommonFlags.HistOutputs = ["OUTPUT:NTupleMaker_1evt.root"]

#Default setup 
from RecExConfig.RecFlags import rec
from TrigT1CaloFexSim.L1SimulationControlFlags import L1Phase1SimFlags as simflags
rec.readAOD=False
rec.readESD=True
rec.readRDO=False
rec.doESD=True
rec.doWriteAOD=False
rec.doHist.set_Value_and_Lock(False)
rec.doCBNT.set_Value_and_Lock(False)
rec.doWriteTAGCOM.set_Value_and_Lock(False)
rec.doWriteTAG.set_Value_and_Lock(False)
rec.doWriteAOD.set_Value_and_Lock(False)
rec.doWriteESD.set_Value_and_Lock(False)
rec.doAOD.set_Value_and_Lock(False)
rec.doMonitoring.set_Value_and_Lock(False)
rec.readAOD.set_Value_and_Lock(False)

'''

simflags.CTP.RunCTPEmulation=False
from TrigT1CaloFexPerf.L1PerfControlFlags import L1Phase1PerfFlags as pflags
pflags.CTP.RunCTPEmulation=False
from RecExConfig.RecFlags import rec
rec.readAOD=True
rec.readESD=True
rec.readRDO=False
rec.doESD=True
rec.doWriteAOD=False

'''


#simflags.Calo.QualBitMask=0x40 
if(doFexAlgos==False):
    simflags.Calo.RunFexAlgorithms=False
    simflags.CTP.RunCTPEmulation=False 

if(isMC==False):
    simflags.Calo.ApplySCQual=False;
    simflags.Calo.SCellType="Emulated"

if(isESD and doFexAlgos):
    include("TrigT1CaloFexPerf/createL1PerfSequence.py")
    include("TrigT1CaloFexSim/createL1SimulationSequence.py")
    #pass
    
if(isMC==False):
    from EnhancedBiasWeighter.EnhancedBiasWeighterConf import EnhancedBiasWeighter
    TRIG1AugmentationTool = EnhancedBiasWeighter(name = "TRIG1AugmentationTool")
    TRIG1AugmentationTool.RunNumber = run_number
    TRIG1AugmentationTool.UseBunchCrossingTool = True
    ToolSvc += TRIG1AugmentationTool

    from DerivationFrameworkCore.DerivationFrameworkCoreConf import DerivationFramework__DerivationKernel
    topSequence += CfgMgr.DerivationFramework__DerivationKernel("TRIG1Kernel",
                                                              AugmentationTools = [TRIG1AugmentationTool]
                                                              )
doMonitoring=False                                                               
if doMonitoring:
    from TrigL1CaloUpgrade.TrigL1CaloUpgradeConf import SimpleSuperCellChecks
    topSequence += SimpleSuperCellChecks() 
    #from TrigL1CaloUpgrade.TrigL1CaloUpgradeConf import SimpleLArDigitsChecks
    #athAlgSeq += SimpleLArDigitsChecks()


####################################
##### Configuration
#####################################
L1_Triggers_list  = []
L1_Triggers_list  += ["L1_XE30", "L1_XE300", "L1_XE35", "L1_XE40", "L1_XE45", "L1_XE50", "L1_XE55", "L1_XE60"]
L1_Triggers_list += ["L1_J100", "L1_J15", "L1_J20", "L1_J40", "L1_J400", "L1_J50"]
#L1_Triggers_list += ["L1_J40_XE50", "L1_J40_XE60"]

HLT_Triggers_list  = []
#HLT_Triggers_list += ["HLT_xe110_mht_L1XE50", "HLT_xe110_pfsum_L1XE50", "HLT_xe110_tc_em_L1XE50", "HLT_xe110_tcpufit_L1XE50"]
#HLT_Triggers_list += ["HLT_xe30_cell_L1XE30", "HLT_xe30_mht_L1XE30", "HLT_xe30_tcpufit_L1XE30", "HLT_xe30_trkmht_L1XE30"]
#HLT_Triggers_list += ["HLT_j100_pf_ftf_bdlr60_xe50_cell_xe85_tcpufit_L1XE55", "HLT_j55_0eta240_cell_L1J30_EMPTY"]
#HLT_Triggers_list += ["HLT_j15", "HLT_j20", "HLT_j25", "HLT_j35", "HLT_j45", "HLT_j60", "HLT_j85", "HLT_j110", "HLT_j175", "HLT_j260", "HLT_j360", "HLT_j400", "HLT_j420", "HLT_j440",]

Triggers_list = L1_Triggers_list + HLT_Triggers_list

HLT_MET_list = ["HLT_xAOD__TrigMissingETContainer_TrigEFMissingET","HLT_xAOD__TrigMissingETContainer_TrigEFMissingET_mht","HLT_xAOD__TrigMissingETContainer_TrigEFMissingET_topocl_PUC"]


Calo_Samplings = [0, 1, 2, 3]

m_useSuperCells = False
m_isMC          = isMC
m_saveAllInfo   = False
m_saveTowers    = False
m_SaveImage     = True
m_saveCells     = True
m_applyNoiseCorrection = False


'''
SCIn = "SCellnoBCID" 


if(isMC):
    SCIn = "SCellnoBCID" 
    if(simflags.Calo.SCellType()=="Emulated"): SCIn = "SimpleSCell" 
    if(simflags.Calo.SCellType()=="BCID"): 
        SCIn = "SCell"
        if("r11881" in tag):
            print tag
            SCIn = "SCellBCID"
    if(simflags.Calo.UseAllCalo()==True): SCIn = "AllCalo"


print(SCIn)
'''

topSequence += CfgMgr.NTupleMaker(CellContainerName = "AllCalo", SCellContainerName = "AllCalo", useSuperCells = m_useSuperCells, saveAllInfo = m_saveAllInfo, saveTowers = m_saveTowers,   HLT_MET = HLT_MET_list, Triggers = Triggers_list, isMC = m_isMC, CaloNoiseTool = theCaloNoiseTool, SaveImage = m_SaveImage, SaveCells = m_saveCells, applyNoiseCorrection = m_applyNoiseCorrection, CaloSampling = Calo_Samplings)    

