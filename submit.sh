samples=(
	#mc16_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.recon.ESD.e3601_s3170_r12399_r12253_r12399
	mc16_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.recon.ESD.e3601_s3126_r12406
	#mc16_13TeV.410471.PhPy8EG_A14_ttbar_hdamp258p75_allhad.recon.ESD.e6337_s3126_r12406
	#mc16_13TeV.361020.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W.recon.ESD.e3569_s3126_r12406
	mc16_13TeV.361021.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1W.recon.ESD.e3569_s3126_r12406
	mc16_13TeV.345058.PowhegPythia8EvtGen_NNPDF3_AZNLO_ggZH125_vvbb.recon.ESD.e6004_e5984_s3126_r12406
	#valid1.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.recon.ESD.e5112_s3227_r12405
)

for sample in "${samples[@]}"
do
	echo $sample
	dsid="$(cut -d'.' -f2 <<<${sample})"
	tag="$(cut -d'.' -f6 <<<${sample})"
	version="V04"
	echo pathena NTupleMaker/jobSettings_Default.py NTupleMaker/jobOptions.py --inDS $sample --outDS=user.mobelfki.$dsid.$tag.SCells_phi.$version --evtMax 1000
	pathena NTupleMaker/jobSettings_Default.py NTupleMaker/jobOptions.py --inDS $sample --outDS=user.mobelfki.$dsid.$tag.SCells_phi.$version --evtMax 1000
done	
